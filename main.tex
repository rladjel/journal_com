
\documentclass{article}
\usepackage{amssymb,amsmath,amsthm}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{parskip}
\usepackage[margin=1in]{geometry}

\usepackage[noend]{algorithmic}
\usepackage[algoruled,algo2e]{algorithm2e}
\usepackage{algorithm}

\input{macros}

\begin{document}

\section{Problem formulation}
\label{problem-formulation}



To be fully generic, our protocol needs to ensure that by observing the communications between nodes, an attacker or a set of colluding attackers is not able to infer personal data. This means that in a data dependent computation where the communications between nodes depends on the data, the attackers should not identify data from specific user.

For example, in a computation of a Group by query over a map-reduce model where each reducer is in charge of a specific group key, by observing the network, an attacker can infer that every mapper
communicating with that reducer has that specific group key. To overcome this limitation, we need to hide the communication patterns.

Hiding the communication pattern (1) should not lead to a massive leak of data. (i.e., in a solution where a single mixer is used to shuffle the
data of all the participant, if an attacker breaks this mixer, the whole dataset is compromised) and (2) should not incur an important overhead
in term of total amount of exchanged data, nor in term of number of remote attestations (number of secure channel opened between nodes) to be performed, and finally (3) should not imply the consent of a huge number of participants.

\section{Naive method}
\label{naive-methods}

\subsection{Broadcast}
\label{broadcast}

For the rest of the paper, two communicating nodes are denoted $s$ and $t$ where $s$ is the \emph{source} node and $t$ is the \emph{target} node. (i.e. in the example of map-reduce, a mapper would be a source node and the reducer would be the target node).\\
A naive way to hide the communication, and the best in terms of privacy, is that each source opens a secure channel with each target and sends his
real value to the target in charge of his key and broadcast a dummy message to every other target. An attacker has now no way of determining which group key a user holds. Unfortunately, this method
incurs a very important communication overhead. Indeed, each user now needs to open a communication channel, perform remote attestations and send a message to every target.
\riad{Changer les figures avec les bonnes notations source/target...etc}

\begin{figure}[ht]
\centering
\includegraphics[width=.6\textwidth]{./media/image01.png}
\caption{Broadcast.}
\label{fig:broadcast}
\end{figure}

\subsection{Mixnet}
\label{mixnet}

Another way to address this problem is to introduce layers of mix network \cite{ref} between users exchanging data. Studies \cite{ref} show that to ensure the same privacy as the broadcast, $\log(n)$ layers are
needed (where $n$ is the number of users). As the path in the layers of mixnet is not known beforehand, each mixnet node from one layer should be able to communicate with every mixnet node from the next layer and the previous layer. This will incur an exponential communication overhead.

Fig.~\ref{fig:broadcast} and \ref{fig:mixnet} show the two architectures, where each connection between two nodes represent a secure channel (which imply two remote
attestations) used to send messages.

\begin{figure}[ht]
\centering
\includegraphics[width=.6\textwidth]{./media/image02.png}
\caption{Mixnet.}
\label{fig:mixnet}
\end{figure}

\subsection{Evaluation of the two methods}
\label{evaluation-of-the-two-methods.}

\subsubsection{Performance analysis}
\label{performance-analysis}

To analyze the effectiveness of the two methods, we consider three metrics, the volume of exchanged data expressed in terms of message unit (i.e., if a node sends data composed of three different messages, it is counted as 3 message units ``mu''), the total number of remote attestation performed between nodes, and the amount of additional users added to the computation. For the three metrics we compare the two methods together with a solution without privacy. The privacy level is the same for the two approaches.

For the sake of clarity and without loss of generality we will analyze the effectiveness of the two methods on an example. Moreover, we consider that all the messages are uniformly distributed over the targets.

Let us consider the computation of a query over a map-reduce model with S sources (mappers) and T targets (reducers).

\paragraph{Volume of exchanged data.}\label{volume-of-exchanged-data}

\emph{Without privacy.} Each source sends only one message to one target and each target sends one message to the Querier. In total $S+T$ mu are transmitted.

\emph{Broadcast.} Each source needs to send one real message to one target and one dummy message to each other target which means $T$
messages for each source. Each target need to send one message to the Querier. In total $ST+T$ mu are sent.

\emph{Mixnet.} Each source sends one message to one mixer from the first layer. As the messages are uniformly distributed, each mixer sends on average $S/K$ messages where $K$ is the width of the mixnet layer. Finally, each target sends one message to the Querier. In total $S+S\log(S)+T$ mu are transmitted.
\begin{figure}[ht]
\centering
\includegraphics[width=.5\textwidth]{./media/image1.png}
\caption{Volume of exchanged data.}
\label{fig:volume1}
\end{figure}

Fig.~\ref{fig:volume1} shows a comparative between the three approaches in term of volume of exchanged data for different value of users. Broadcast and mixnet are by far more expensive than without privacy: the total volume of transmitted data is ten to fifty times bigger.

\paragraph{Number of remote attestations.}
\label{number-of-remote-attestations}

\emph{Without privacy}. Each source needs to attest himself to one target only. Each target needs to attest himself to every source communicating with it which is on average around $S/T$. In total $2S$ remote attestation are done.

\emph{Broadcast}. Each source needs to attest himself to each target. In the other side, each target need to attest himself to each source. This means that the total number of remote attestations is $2ST$.

\emph{Mixnet}. Each source needs to attest himself to one mixer from the first layer of mixnet. Then, each mixer from the first layer has to attest himself to each mixer from the second layer (i.e. $K$) additionally to the attestations with the sources communicating with it which is on average around $S/K$. Each mixer from the other layers except from the last one needs to attest himself to the $K$ mixers from the previous layer and the $K$ mixers from the next layer. The mixers from the last layers need to attest themselves to every mixer from the previous layer ($K$) and every target ($T$). Finally, each target needs to attest himself to every mixer from the last layer ($K$). In total, $2S+2K^2+2(\log(S)-2)K^2+2TK$.

\begin{figure}[ht]
\centering
\includegraphics[width=.5\textwidth]{./media/image2.png}
\caption{Number of remote attestations}
\label{fig:attestations1}
\end{figure}


Fig.~\ref{fig:attestations1} shows a comparative between the three approaches in term of number of remote attestations done between nodes for different values of sources while the number of target is fixed to 50 and the width of the mixnet layer is fixed to 100. These values may seem arbitrary but the objective here is not to tune an actual execution of a query but to show a simple comparative between different methods. The analysis shows that the mixnet is better than the broadcast but still incurs an important overhead compared to without privacy.

\paragraph{Number of additional consents.}\label{number-of-additional-consents.}

The broadcast method does not incur any additional number of consents. Indeed, there is no more node added to the computation. For the Mixnet method, one need new nodes to play the role of mixers. This number depends on the number of mixers' layers ($\log(S)$) and the width of each layer $K$. We have then $K\log(S)$ additional consent to gather.

\begin{figure}[ht]
\centering
\includegraphics[width=.5\textwidth]{./media/image3.png}
\caption{Number of additional consents}
\label{fig:consents1}
\end{figure}

Fig.~\ref{fig:consents1} shows the number of additional consents needed to apply the solutions. Only the mixnet solution incurs an additional number.

\subsubsection{Privacy analysis}\label{privacy-analysis}

As said before, the two methods are equivalent in term of privacy. In the broadcast the attacker observing the network cannot distinguish if a communication between two nodes is a real message or a dummy. For the mixnet, the attacker cannot link the output to the input.

\section{Relaxing privacy constraint}\label{relaxing-privacy-constraint}

In order to reduce the overhead, we propose a solution which trades privacy for performance. The classical approach to quantify the privacy loss is differential privacy \cite{ref-dp}.
More specifically, in this section we embrace the local model of differential privacy \cite{ref-ldp}, where each private input is randomized locally by its owner before sharing it. Local differential privacy (LDP) is known to be a very strong model of privacy, which provides participants with plausible deniability of their private data.

\subsection{Formal definitions}

\aurelien{the paragraph below needs to be improved. this is where we want to mention that each input is a actually a triplet $(s,m,t)$ but that the message itself is encrypted so we can model our privacy guarantees based only on communication patterns.}
We now describe how we adapt the framework of LDP to our context.
% We need first the adapt the differential privacy to our context.
Indeed, in our case, the integrity of the final result should be kept together with the threshold of participating users. Crucially, we consider that inputs are the communication targets, not the data itself.
% This implies that the noise added to satisfy LDP will have no effect on the final result.
%(i.e. instead of sending fake data, a user sends a dummy message easily identifiable by the target which will be removed before computing the final result).
% This noise has two effects. First, it compensates the privacy lost when reducing the number of dummies.
% Second, it gives means to users to deny their participation to the
% computation.

\textbf{Inputs.} In our context, an input is a couple $(s,t)$ where $s \in \Source$ is the source and $t \in \Target$ is the target. In a map-reduce setting, $(s,t)$ means that source $s$ holds a key managed by target $t$.

\textbf{Local randomizer.} A local randomizer $\Randomizer$ takes an input $(s,t)$ and outputs a (random) multiset $O=\{(s_j,t_j)\}$ where $s_j\in \Source_{o}$ and $t_j\in \Target_{o}$ (with $\Source\subseteq \Source_{o}$ and  $\Target\subseteq\Target_{o}$).

\textbf{$\epsilon$-local differential privacy.} A local randomizer $\Randomizer$ is $\epsilon$-locally differentially private if for any source $s\in\Source$, any pair $t,t'\in\Target$ of targets and any subset $\Output$ of possible outputs of $\Randomizer$ we have:
\[
    \frac{P[\Randomizer((s,t)) \in \Output]}{P[\Randomizer((s,t')) \in \Output]} \leq e^{\epsilon}.
\]

\subsection{Proposed algorithm}

Our algorithm for the local randomizer combines the idea of randomized response \cite{rand-resp} (where each input is randomized according to a probability of lying) together with the ability to send additional \emph{dummy} messages.
Let $\Plie$ be the probability of lying. First, each source
sends his real value to his actual target with a probability
$(1-\Plie)$ or a dummy to a random target (different from
his original one) with probability $\Plie$. Second, each source sends $d$ dummies to $d$ random targets (different from his original one). Note that adding dummies will not affect the final result, as dummies are discarded before producing the final result.
The resulting local randomizer is summarized in Algorithm~\ref{algo:noise-mapper}, where we denote a dummy value by $\dummy$. Each source runs this algorithm for each message that they need to send.

%\aurelien{We also discussed this, but it is not clear to me why you prevent sending a dummy to the correct reducer when the user lied. This would avoid the odd behavior you have in Fig.~\ref{fig:dp1} (as sending more dummies will not reveal more info) and it would increase utility a bit (as one might as well send the actual input when the correct reducer is selected in the dummy phase). Am I missing something?}
%\riad{The privacy level is slightly better when doing like this, see Fig.~\ref{fig:comparison} which shows the difference between the two versions for a fixed $\Plie=0.5$ Version 1 is the paper one and Version 2 is the one you're describing. If you take for example 18 dummies, the epsilon for the actual version is around 0.5 while for the version you're describing is around 1 }
%\aurelien{thanks. on the other hand, version 2 has better utility because as the number of dummies increases the number of additional consents goes to zero}
%\gs{We should probably normalize Plie in version 1 so we have the same utility as version 2. Also explain why version is better and gets to 0}

\begin{figure}[ht]
\centering
\includegraphics[width=.7\textwidth]{./media/image10''.png}
\caption{Comparison between the two versions for $\Plie=0.5$.}
\label{fig:comparison2}
\end{figure}

% \nicolas{TODO riad : tracer la fig 6 avec une même proba d'envoyer le vrai message dans le but de savoir trancher entre verison 1 verison 2}
%\riad{La courbe decrite ci-dessus par Nico me pose problème. En faisant les calculs je suis tombé sur des résultats que je n'arrive pas à comprendre. La probabilité d'envoyer le vrai message est : 

%\begin{align*}
%    P(\text{vrai})&=P(\text{Dire la vérité}) + P(\text{Mentir et tirer le bon reducer}) \\
%    P(\text{vrai})&=(1-\Plie)+  \frac{\Plie \times (d+1)}{T}
%\end{align*}
  
%Nous on veut un $\Plie$ qui varie en fonction de $d$ pour avoir tout le temps la même probabilité d'envoyer le vrai message (i.e. %P(\text{vrai}) fixée). On a :

%$$ \Plie = \frac{T\times (1-P(\text{vrai}))}{T-(d+1)} $$ 
%Si je prends les même paramètre de la courbe ci-dessus, le $\Plie$ augmente quand on a plus de dummies alors qu'il est censé diminuer. Pire encore il est égale à $1$ à 24 dummies et plus grand que $1$ au delà. Je me doute bien qu'il y a une erreur dans mon raisonnement mais je n'arrive pas à la trouver...}

\nicolas{PlieA = version aurélien, PlieR = version riad. Il faut tracer la courbe de la version Aurelien avec PlieA=PlieR(1-T/d). Donc si dans la version riad PlieR=0.5, tracer la version aurelien avec pour chaque point d'abscisse "d" le epsilon pour un PlieA = 0,5*T/(T-d+1)
Donc : même résultats avec les 2 algos. L'introduire sur la verison aurelien, et l'exprimer sous forme de changement de variabe avec la verison riad car simplifie la suite.}
\aurelien{ou bien ne présenter que la version riad, mais ajouter une remarque expliquant en quoi c'est équivalent avec la version aurélien tout en étant plus facile de travailler avec}

This way of adding noise preserves the integrity of the final result but as $S \times \Plie$ messages are dummies, the final result will not satisfy anymore the threshold of participating users. To overcome this problem, we need to add more users to the computation. The new number of sources is $S_t=S/(1-\Plie)$.

\begin{algorithm2e}
  \DontPrintSemicolon
  % \LinesNumbered
  \SetKwComment{Comment}{{//\ \scriptsize}}{}
  \newcommand{\comr}[1]{\Comment*[r]{#1}}
  \caption{Local randomizer (run on the source)}
  \label{algo:noise-mapper}
  \KwIn{$\Plie$ probability of lying, $\Target$ list of possible targets, $s\in\Source$ identity of the source, $t\in\Target$ desired target of the source $s$, $m$ the message for this target, $d<T$ number of dummies.}
  \KwOut{$O=\{(s,t_j)\}$ where $t_j\in \Target$, the communication pattern (i.e. what the adversary may observe)}
  \aurelien{to update as discussed: remove output, and instead define it properly outside the algorithm}
  \BlankLine
 $rnd \leftarrow random(0,1)$ \comr{generate random number between 0 and
  1}
  \eIf{$rnd < \Plie$}{
  $\Target.remove(t)$ \comr{remove correct target from list of targets}
  $t_{tmp} \leftarrow choose\_random(\Target)$ \comr{pick random target}
  $Com.add(( \dummy,t_{tmp}))$ \comr{random target $t_{tmp}$ will get a dummy value}
  $O.add((s, t_{tmp}))$ \comr{add communication pattern to output}
  $\Target.remove(t_{tmp})$ \comr{remove selected target from list}
  }{
  $Com.add((m,t))$ \comr{correct target will get the correct message}
  $O.add((s,t))$ \comr{add communication pattern to output}
  $\Target.remove(t)$
  }
  \For{$i=0$ \textbf{\emph{to}} $i < d$}{
  $t_{tmp} \leftarrow choose\_random(\Target)$\;
  $Com.add(( \dummy,t_{tmp}))$ \comr{random target $t_{tmp}$ will get a dummy value}
  $O.add((s,t_{tmp}))$ \comr{add communication pattern to output}
  $\Target.remove(t_{tmp})$
  }
  \textbf{send} $Com$\comr{send messages in random order to appropriate targets}
  \Return{$O$} \comr{communication pattern is returned to the adversary}
\end{algorithm2e}

\subsection{Evaluation}\label{evaluation}

\subsubsection{Performance}\label{performance}

To evaluate the effectiveness of the solution we use the same metrics as before. We also compare the solution to the naive ones. In all experiments below, we use $S=10000$, $T=50$.

\paragraph{Volume of data.}\label{volume-of-data}

Each source needs to send either one real message to one target and $d$ dummies to $d$ other targets or $d+1$ dummies to $d+1$ random target. which means $d+1$ mu for each source. Each target need to send one message to the Querier. In total $S_t(d+1)+T$ mu
are transmitted. See Fig.~\ref{fig:volume2}.

\begin{figure}[ht]
\centering
\includegraphics[width=.7\textwidth]{./media/image4.png}
\caption{Volume of transmitted data for different value of
$\Plie$.}
\label{fig:volume2}
\end{figure}

\paragraph{Number of remote attestations.}\label{number-of-remote-attestations-1}

Each source needs to attest himself to $d+1$ targets. In the other side, each target need to attest himself to $S_t(d+1)/T$ sources. This means that the total number of remote attestations is $2 S_t (d+1)$. See Fig.~\ref{fig:attestations2}.

\begin{figure}[ht]
\centering
\includegraphics[width=.7\textwidth]{./media/image5.png}
\caption{Number of remote attestation for different value of
$\Plie$.}
\label{fig:attestations2}
\end{figure}

\paragraph{Number of additional consents.}\label{number-additional-consents}

As some users sends dummies instead of their real value (due to
$\Plie$), the number of participating user should be increased to satisfy the threshold specified in the manifest. The new
number of users is $S/(1-\Plie)$. This means that this
solution incurs $S/(1-\Plie)-S$ additional consents. See Fig.~\ref{fig:consents2}.

\begin{figure}[ht]
\centering
\includegraphics[width=.5\textwidth]{./media/image6.png}
\caption{Number of additional consents needed for different values of $\Plie$.}
\label{fig:consents2}
\end{figure}

\subsubsection{Privacy}\label{privacy}

Let us now analyze to which extent our algorithm satisfies local differential privacy.

\begin{theorem}
Algorithm~\ref{algo:noise-mapper} is $\epsilon$-locally differentially
private with 
$$\epsilon=\ln\left( \frac{( 1 - \Plie) ( T - d - 1 )}{\Plie ( d + 1 )} \right).$$
\end{theorem}

\begin{proof}
%Without loss of generality, we consider that our two neighboring datasets $\Dataset_{1}$ and $\Dataset_{2}$ differ on a couple involving source $s_1$: we denote by $t_1$ the corresponding target in $\Dataset_{1}$ and by $t_1'$ the corresponding one in $\Dataset_{2}$.

Let $(s,t)$ and $(s,t')$ be two arbitrary inputs.
Let $O$ be the output of an actual execution of $\Randomizer$. We can distinguish three cases:
\begin{itemize}
\item $t\in O$ and $t' \in O$,
\item $t\notin O$ and $t' \notin O$,
\item $t\in O$ and $t' \notin O$ (or inverted).
\end{itemize}

The two first ones have the same probability regardless of whether the input target was $t$ or $t'$, so we can focus on the last case:
\begin{align*}
P[\Randomizer((s,t)) = O] &= P[s\text{ says\ the\ truth}] \times P[s\text{ does not send a dummy to }t']\\
&= ( 1 - \Plie ) \left( 1 - \frac{d}{T - 1} \right),\\
P[\Randomizer((s,t')) = O] &= P[s\text{ lies} ] \times P[ s\text{ sends a dummy to }t]\\
&= \Plie \left( \frac{d + 1}{T - 1} \right).
\end{align*}
Hence for any $s\in\Source$, $t,t'\in\Source$ and subset of outputs $\Output$:
$$\frac{P[\Randomizer((s,t)) \in \Output]}{P[\Randomizer((s,t')) \in \Output]} = \frac{( 1 - \Plie) ( T - d - 1 )}{\Plie ( d + 1 )}.$$
\end{proof}

\begin{figure}[ht]
\centering
\includegraphics[width=.6\textwidth]{./media/image7.png}
\caption{Value of $\epsilon$ for different values of $\Plie$ and number of dummies.}
\label{fig:dp1}
\end{figure}

Fig.~\ref{fig:dp1} shows the evolution of epsilon for different number of dummies, and fixed parameters $S=10000$, $T=50$. The bigger $\Plie$ is the faster the epsilon reach 0 and then increases again, this is due to the fact that when the number of dummies is too big, it reveals some information.

\subsubsection{Conclusion}\label{conclusion}

This solution gives a relatively good privacy for some use cases where the initial number of participants is not big while keeping a reasonable overhead. But it becomes quickly impracticable for use cases where one need tens of thousands participants.

\section{Mutualizing dummies}\label{mutualizing-dummies}

\aurelien{clarify that at this point, the scramblers become the sources}

\subsection{Introduction}\label{introduction-1}

\riad{Spécifier que le max du nombre de message envoyer à une target est égale à la taille du Buffer }

To bypass the limitation of the first solution, we propose a second
solution which mutualize the dummies for many users. In this solution we introduce a layer of scrambler between the sources and targets. The role of the scrambler is to gather a certain amount of messages from different sources, shuffles them and forward them to the original target. We denote $\Buffer$ the batch of messages received by the scrambler and $B$ the amount of messages. The noise is added as follows.

First, each source sends his real value with a probability $(1-\Plie)$ or a dummy with probability $\Plie$ to a random scrambler. In both cases the message is encrypted with the public key of the target, which means that the scrambler can not see the messages. Second, when the buffer is full, each scrambler sends the received messages together with $d$ dummies to $d$ random targets. The total number of messages that a scrambler sends to the same target is capped to the number of messages that it received from the sources, as sending more would not improve the privacy.

Algorithm~\ref{algo:noise-scrambler} describes the procedure, abstracting away the sources for now (scramblers have access to the true inputs of sources). 


\begin{algorithm2e}
  \DontPrintSemicolon
  % \LinesNumbered
  \SetKwComment{Comment}{{//\ \scriptsize}}{}
  \newcommand{\comr}[1]{\Comment*[r]{#1}}
  \caption{Noise addition (run by the scrambler)}
  \label{algo:noise-scrambler}
  \KwIn{$\Plie$ probability of lying, $\Target$ list of possible targets,  $s\in\mathcal{S}$ identity of the scrambler, $\Buffer = \{ ( m_i,t_{i} ) \}$ a set of $B$ tuples to send, $d$ number of dummies.}
  \aurelien{here also there should be an upper bound on the number of dummies, something like $T(B-1)$?}
  \riad{If I recall well, at the beginning we had an upper bound around $B+d<T$ and another condition $d<B$ or smth like this, but our algorithms changed many times since then, we'll probably have a more accurate bound when we'll have the evaluation part}
 
  \KwOut{$O=\{(s,t_j)\}$ where $t_j\in \Target$, the communication pattern (i.e. what the adversary may observe)}
  
 
  \BlankLine
  \ForEach{$(m,t)$ \textbf{\emph{in}} $\Buffer$}
  {
      $rnd \leftarrow random(0,1)$\;
      \eIf{$rnd < \Plie$}
      {
          $t_{tmp} \leftarrow choose\_random(\Target \setminus t)$\;
          $Com.add(( \dummy,t_{tmp}))$\;
          $O.add(( s,t_{tmp}))$
      }
      {
        $Com.add((m,t))$\;
        $O.add((s,t))$
      }
  }
  \For{$i=0$ \textbf{to} $i < d$}
  {
      $t_{tmp} \leftarrow choose\_random(\Target)$\;
      $Com.add((\dummy,t_{tmp}))$\;
      $O.add((s,t_{tmp}))$\;
      \If{$O.count(t_{tmp})==B$}
      {
        $\Target.remove(t_{tmp})$
      }
  }
  \textbf{send} Com\;
  \Return{$O$}
\end{algorithm2e}

\subsection{Evaluation}\label{evaluation-1}

Again, in all experiments below we use $S=10000$, $T=50$. The number of scramblers is defined by the size of the buffer and the total number of sources: it is given by $S_t /B$.


\subsubsection{Performance}\label{performance-1}

\paragraph{Volume of data.}\label{volume-of-data-1}

Each source needs to send one message to one scrambler. Each scrambler needs to send $B+d$ messages and each target need to send one message. In total $S_t + (S_t /B)(B+d)+T$ mu are
transmitted. See Fig~\ref{fig:volume3}.

\begin{figure}[ht]
\centering
\includegraphics[width=.7\textwidth]{./media/image8.png}
\caption{Volume of transmitted data for different value of
$\Plie$.}
\label{fig:volume3}
\end{figure}


\paragraph{Number of remote
attestations}\label{number-of-remote-attestations-2}

Each source needs to attest himself to one scrambler. Each scrambler needs to attest himself to $B$ sources and every target. Each target need to attest himself to $S_t/B$ scrambler. This means that the total number of remote attestations is $S_t+(B+T)S_t/B +
S_t/BT$. See Fig~\ref{fig:attestations3}.

\begin{figure}[ht]
\centering
\includegraphics[width=.6\textwidth]{./media/image9.png}
\caption{Number of remote attestation for different value of
$\Plie$.}
\label{fig:attestations3}
\end{figure}


\subsubsection{Privacy}\label{privacy-1}

Unlike our first solution, Algorithm~\ref{algo:noise-scrambler} takes as input a communication dataset rather than a single couple. Each scrambler acts as a trusted curator which collects a subset of the inputs from the original sources and randomizes them. We thus consider here the global model of differential privacy and define an appropriate notion of neighboring datasets.

\textbf{Communication dataset.} A communication dataset is a multiset of $n$ couples $\Dataset=\{(s_{i},t_{i})\}_{i=1}^n$ where source
$s_i \in \Source$ and  target $t_i \in \Target$. In a map-reduce setting,
$(s_{i},t_{i})$ means that the source $s_{i}$ holds a key managed by the target $t_{i}$.

\textbf{Neighboring datasets.} Two datasets
$\Dataset_1=\{(s_i,t_i)\}_{i=1}^n$ and
$\Dataset_2=\{(s_i,t_i')\}_{i=1}^n$ are said neighbors if there exists a unique $i$ such that $t_i \neq t_i'$ (i.e. exactly one source communicates with a different target).

\gs{Maybe add a name to this type of algo, it looks like we're redifining what an algo is}
\textbf{Outputs.} An algorithm $\Algo$ takes as input a dataset $\Dataset$ and outputs a multiset $O=\{(s_j,t_j)\}$ where $s_j\in \Source_{o}$ and $t_j\in \Target_{o}$ (with $\Source\subseteq \Source_{o}$ and  $\Target\subseteq\Target_{o}$).


\textbf{$\epsilon$-differential privacy.} A (randomized) algorithm $\Algo$ is said
$\epsilon$-differentially private if for every neighboring datasets $\Dataset_{1}$ and
$\Dataset_{2}$ and for each subset $\Output$ of possible outputs of $\Algo$ we have:
\[
    \frac{P[\Algo(\Dataset_{1}) \in \Output]}{P[\Algo(\Dataset_{2}) \in \Output]} \leq e^{\epsilon}
\]

Let us now analyze if Algorithm~\ref{algo:noise-scrambler}
is differentially private. As for the previous case we consider two neighboring datasets $\Dataset_1$ and $\Dataset_2$. Let $O$ be
the observed result of an actual execution (i.e. $O \in \Output$).
%We want to know to which $\epsilon$ our algorithm is $\epsilon$-differentially private.

Without loss of generality, we consider that our two neighboring datasets $\Dataset_{1}$ and $\Dataset_{2}$ differ on a couple involving source $s_1$: we denote by $t_1$ the corresponding target in $\Dataset_{1}$ and by $t_1'$ the corresponding one in $\Dataset_{2}$.
The output of Algorithm~\ref{algo:noise-scrambler} depends on the buffer $\Buffer$ and the number of dummies $d$.
For the sake of clarity, we denote $\Buffer_{n}$ a buffer of size $n$ and $O_{n}$ an output of size $n$.
%As the scrambler can not see the content of the message, and this message does not have any impact on the privacy, one can abstract the buffer as follows:
As the scrambler keeps the original source of each message secret, one can abstract the buffer as 
$\Buffer_{n} = \{t_i\}_{i=1}^{n}$ where $t_i \in \Target$. In the same way, one can abstract the output as $O_{n} = \{t_i\}_{i=1}^{n}$.

%\textbf{Buffer output.} $O_{n} = \{ y_{1},y_{2}\ldots,y_{n }\}$ is the output of a buffer where $y_{i} \in \{ r_{1},r_{2},\ldots,r_{R} \}$.


For the two neighboring datasets $\Dataset_{1}$ and $\Dataset_{2}$, all the buffers are equal except from one, the buffer that receives the message from $s_{1}$. We can thus consider:
$\Dataset_{1} = \Buffer_{n} = \{t_1\}\cup \{t_{i}\}_{i=2}^{n}$ and
$\Dataset_{2} = \Buffer_{n}^{'} = \{t_1'\} \cup \{t_{i}\}_{i=2}^{n}$, where each $t_{i}$ can be any target in $\Target$.
% either $t_{i},t_{j}$ or any target $t_{k}$ different from both.
%\{(s_{i},t_{i})\}_{i=1}^n$ where source
%$s_i \in \Source$ and  target $t_i \in \Target$.


To evaluate the algorithm, we need first to determine what is the worst output (i.e. the output where $\epsilon$ is highest).

\begin{lemma} Let us first consider the case without dummies ($d=0$). If $\Plie < \frac{T - 1}{T}$ then:
$$\forall O_n \,\Buffer_{n},\Buffer_{n}^{'},\quad \frac{P[ \Algo( \Buffer_{n} ) = O_n ]}{P[ \Algo( \Buffer_{n}' ) = O_n ]} \leq \frac{P\left[ \Algo( \Buffer_{n} ) = \{t_1\}_{i=1}^{n} \right]}{P\left[ \Algo( \Buffer_{n}' ) = \{t_1\}_{l=1}^{n} \right]}.$$
\end{lemma}
\begin{proof}
Let us prove the lemma by induction.



For $n = 1$ we have (ignoring symmetric cases):
\begin{align}
\frac{P[ \Algo( \{ t_1\} ) = \{ t_1 \} ]}{P[ \Algo( \{ t_1' \} ) = \{ t_1 \} ]} &= \frac{P[s_1\text{ says the truth}]}{P[ s_1\text{ lies and chooses }t_1 ]} = \frac{1 - \Plie}{\frac{\Plie}{(T - 1)}} = \frac{( 1 - \Plie )(T - 1)}{\Plie},\label{eq:t1_t1}\\
\frac{P[ \Algo( \{ t_1\} ) = \{ t_1' \} ]}{P[ \Algo( \{ t_1' \} ) = \{ t_1' \} ]} &= \frac{P[ s_1\text{ lies and chooses }t_1' ]}{P[ s_1\text{ says the truth} ]} = \frac{\frac{\Plie}{(T - 1)}}{1 - \Plie} = \frac{\Plie}{( 1 - \Plie )( T - 1 )},\label{eq:t1_t1p}\\
\frac{P[ \Algo( \{ t_1\} ) = \{ t_i \} ]}{P[ \Algo( \{ t_1' \} ) = \{ t_i' \} ]} &= \frac{P[ s_1\text{ lies and chooses }t_i ]}{P[ s_1\text{ lies and chooses }t_i ]} = 1\text{ for any }t_i\text{ such that } t_i\neq t_1,t_i\neq t_1'.\nonumber
\end{align}

Since $\Plie < \frac{T - 1}{T}$, Eq. \eqref{eq:t1_t1} is larger than Eq. \eqref{eq:t1_t1p}.

We now assume that:

$$\forall O_n,\Buffer_{n},\Buffer_{n}^{'},\quad \frac{P[ \Algo( \Buffer_{n} ) = O_n ]}{P[ \Algo( \Buffer_{n}' ) = O_n ]} \leq \frac{P\left[ \Algo( \Buffer_{n} ) = \{t_1\}_{i=1}^{n} \right]}{P\left[ \Algo( \Buffer_{n}' ) = \{t_1\}_{i=1}^{n} \right]}.$$



Denoting $\alpha_{n}=\frac{P[ \Algo( \Buffer_{n} ) = \{t_1\}_{i=1}^{n} ]}{P[ \Algo( \Buffer_{n}' ) = \{t_1\}_{i=1}^{n} ]}$, this property is equivalent to
$$\forall O_n,\Buffer_{n},\Buffer_{n}^{'},\quad P[ \Algo( \Buffer_{n} ) = O_{n} ] \leq \alpha_{n} P[ \Algo( \Buffer_{n}' ) = O_{n} ].$$

Let us prove that the property holds for $n + 1$.
\nicolas{revoir les notations}
Let $\Buffer = \Buffer_{n+1} \setminus \{t_{n+1}\}$, $\Buffer' = \Buffer_{n+1}' \setminus \{t_{n+1}\}$. Let $(O_{n}^i)_i$ be the family of subsets of size $n$ of $O_{n+1}$. We have the following equalities:
\begin{align}
P[ \Algo( \Buffer_{n + 1} ) = O_{n + 1} ] &= 
\sum_{i=1}^{n+1}{\lambda_{i}P[ \Algo( \Buffer ) = O_{n}^i}]\\
P[ \Algo( \Buffer_{n + 1}' ) = O_{n + 1} ] &= 
\sum_{i=1}^{n+1}{\lambda_{i}P[ \Algo( \Buffer' ) = O_{n}^i}]
\end{align}
with $\lambda_i = P[\Algo(\{t_{n+1}\}) = O_{n+1} \setminus O_n^i]$.

By induction (as $|\Buffer| = |\Buffer'| = n$), we know that
$\forall i\in\{1,\dots,n\}$, we have $P[ \Algo( \Buffer ) = O_{n}^i ] \leq \alpha_{n}P[ \Algo( \Buffer' ) = O_{n}^i ].$
We then have:
\begin{align}
P[ \Algo( \Buffer_{n + 1} ) = O_{n + 1} ] & \leq \sum_{i=1}^n{\lambda_{i}\alpha_{n}P[ \Algo( \Buffer') = O_{n}^i ]}\nonumber\\
& = \alpha_{n}\sum_{i=1}^n{\lambda_i P[ \Algo( \Buffer') = O_{n}^i ]}\nonumber\\
& = \alpha_{n}P[ \Algo( \Buffer_{n + 1}' ) = O_{n + 1} ]\label{eq:Bn_plus_1_ratio}.
\end{align}

Moreover,
\begin{align}
\alpha_{n + 1} &= \frac{P[ \Algo( \Buffer_{n + 1} ) = \{t_1\}_{i=1}^{n+1} ]}{P[ \Algo( \Buffer_{n + 1}' ) = \{t_1\}_{i=1}^{n+1} ]}= \frac{P[\Algo(\{t_1\} ) = \{ t_1 \}]\prod_{i=2}^{n}{P[\Algo( \{t_{i}\} ) = \{ t_1 \}]}}{P[\Algo( \{t_1'\} ) = \{ t_1 \}]\prod_{i=2}^{n}{P[\Algo( \{t_i\} ) = \{ t_1 \}]}} = \alpha_{n}. \label{eq:alpha_n_plus_1}
\end{align}




Combining Eq. \eqref{eq:Bn_plus_1_ratio} and \eqref{eq:alpha_n_plus_1} gives
$$\frac{P[ \Algo( \Buffer_{n + 1} ) = O_{n + 1} ]}{P[ \Algo( \Buffer_{n + 1}' ) = O_{n + 1} ]} \leq \alpha_{n + 1},$$
which concludes the proof of the theorem.
\end{proof}

\subsection{Pushing randomization to the sources}

\aurelien{We should point out that even if the scrambler is compromised, global DP guarantees hold as long as the scrambler is not compromised. If it is compromised, then some LDP guarantees will still hold.}

\aurelien{Note: this could motivate a hybrid protocol where mappers can also choose to send dummies to scramblers as in the first protocol, to improve their LDP guarantees?}

\end{document}
